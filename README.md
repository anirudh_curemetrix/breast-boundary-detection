This is the readme file for running the breast-boundary detection algorithms.
The Algorithms is written in jupyter notebook wiht Python 2.7
Requirements for running the files: 
1. Python 2.7
2. OpenCV 4.0.0
3. Numpy 

The following algorithms are tested on Breast Images without any type of clamps. The images used are from "/nfs/images/PNGs_2048x1600_maxwell/Normal " 
Both of the algorithm use automatic thresholding, so we dont need to find one particular threshold for all Images


Method 1: Breast Boundary detection using Edge detection 
To see output at individual stages,
use the following command 
plt.imshow(A)
A - InputImage, thresh, floodfill_output, backtorgb

Method 2: Breast Boundary detection using Contours
To see output at individual stages,
use the following command 
plt.imshow(A)
A - InputImage, thresh, floodfill_output, img